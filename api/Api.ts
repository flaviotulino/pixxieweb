import config from "./config";
/**
 * This class provides methods to fetch and send server data
 */

export abstract class Api {
    static $inject = ['$http'];
    protected BASE_URL;

    constructor(private $http) {
        // Configure the endpoint basing on the running environment
        this.BASE_URL = window.location.hostname == "localhost" ? config.DEV_ENDPOINT : config.PRODUCTION_ENDPOINT;
    }

    /**
     * Makes an HTTP request using the get method providing
     * url interpolation with the BASE_URL
     * @param url String
     * @returns {any}
     */
    protected get(url:string):angular.IHttpPromise<any> {
        return this.$http.get(this.BASE_URL + url);
    }

    /**
     * Makes an HTTP request using the post method providing
     * url interpolation with the BASE_URL
     * @param url String
     * @param data Object
     * @returns {any}
     */
    protected post(url:string, data:any):angular.IHttpPromise<any> {
        return this.$http.post(this.BASE_URL + url, data)
    }

    /**
     * Makes an HTTP request using the put method providing
     * url interpolation with the BASE_URL
     * @param url String
     * @param data Object
     * @returns {any}
     */
    protected put(url:string, data:any):angular.IHttpPromise<any> {
        return this.$http.put(this.BASE_URL + url, data);
    }


    /**
     * Makes an HTTP request using the delete method providing
     * url interpolation with the BASE_URL
     * @param url String
     * @returns {any}
     */
    protected delete(url:string):angular.IHttpPromise<any> {
        return this.$http.delete(this.BASE_URL + url);
    }


}
