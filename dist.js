var fs = require('fs');
var glob = require("glob");


/**
 * This function returns the dist folder itself appending the file
 * @param target
 * @returns {*}
 */
function dist(target) {
    if (target != undefined && target != null) {
        return 'dist/' + target;
    } else {
        return 'dist';
    }

}

/**
 * Checks if a folder exists and delete it
 * @param dir
 */
function removeDirIfExsists(dir) {
    if (fs.existsSync(dir)) {
        fs.readdirSync(dir).forEach(function (file, index) {
            var curPath = dir + "/" + file;
            if (fs.lstatSync(curPath).isDirectory()) { // recurse
                removeDirIfExsists(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(dir);
    }
}


// Clean up dist/ folder
removeDirIfExsists(dist());
fs.mkdirSync(dist(), '777');

// Clean up dist/lib/ folder
removeDirIfExsists(dist('lib'));
fs.mkdirSync(dist('lib'), '777');

/*          INDEX FILE MANIPULATION         */
var indexFile = fs.readFileSync('index.html', 'utf-8');

// Get all scripts tags and their source
var scriptTags = indexFile.match(/<script.*?src='(.*?)'/gi) || indexFile.match(/<script.*?src="(.*?)"/gi);

scriptTags.map(function (script) {
    // Remove single qoutes and double ones
    script = script.replace(/\<script src=/gi, '').replace('"', '').replace('"', '');

    // Get the filename without the path
    var scriptName = script.slice(script.lastIndexOf('/') + 1, script.length);

    // Copy the file under dist/lib/ folder
    fs.writeFileSync(dist('lib/' + scriptName), fs.readFileSync(script));

    // Replace the source of all script tags with the one with lib/
    var pathRegex = new RegExp(script);
    indexFile = indexFile.replace(pathRegex, 'lib/' + scriptName);
});

// Write the manipulated index file
fs.writeFileSync(dist('index.html'), indexFile);


removeDirIfExsists(dist('assets'));
fs.mkdirSync(dist('assets'), '777');

removeDirIfExsists(dist('assets/javascripts'));
fs.mkdirSync(dist('assets/javascripts'), '777');

removeDirIfExsists(dist('assets/javascripts/controllers'));
fs.mkdirSync(dist('assets/javascripts/controllers'), '777');


removeDirIfExsists(dist('assets/javascripts/direcrives'));
fs.mkdirSync(dist('assets/javascripts/directives'), '777');

removeDirIfExsists(dist('assets/javascripts/services'));
fs.mkdirSync(dist('assets/javascripts/services'), '777');



glob('assets/javascripts/**/*.js', {}, function (er, files) {
    files.map(function (file) {
        fs.writeFileSync(dist(file), fs.readFileSync(file));
    })
});


// Stylesheets
removeDirIfExsists(dist('assets/stylesheets'));
fs.mkdirSync(dist('assets/stylesheets'), '777');
fs.writeFileSync(dist('assets/stylesheets/application.css'), fs.readFileSync('assets/stylesheets/application.css'));



