System.register(["./controllers/LoginController"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var LoginController_1;
    var Routes;
    return {
        setters:[
            function (LoginController_1_1) {
                LoginController_1 = LoginController_1_1;
            }],
        execute: function() {
            Routes = (function () {
                function Routes() {
                }
                Routes.routes = [
                    {
                        name: 'login',
                        url: '/login',
                        controller: LoginController_1.default
                    },
                    {
                        name: 'dashboard',
                        url: '',
                        templateUrl: '/views/dashboard.html',
                        abstract: true
                    },
                    {
                        name: 'dashboard.home',
                        url: '/'
                    }
                ];
                Routes.otherwise = "/";
                return Routes;
            }());
            exports_1("default",Routes);
        }
    }
});
//# sourceMappingURL=routes.js.map