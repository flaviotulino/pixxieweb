import I18n from "./I18n";
function run($rootScope, $http) {
    $rootScope.t = I18n;

    $http.get('locales/' + navigator.language + ".bundle.json").then(res => {
        $rootScope.l = res.data;
    });

    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
        $rootScope.page = toState.name;
    })

}
run.$inject = ['$rootScope', "$http"];
export default run;