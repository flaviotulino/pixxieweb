System.register(["./I18n"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var I18n_1;
    function run($rootScope, $http) {
        $rootScope.t = I18n_1.default;
        $http.get('locales/' + navigator.language + ".bundle.json").then(function (res) {
            $rootScope.l = res.data;
        });
        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            $rootScope.page = toState.name;
        });
    }
    return {
        setters:[
            function (I18n_1_1) {
                I18n_1 = I18n_1_1;
            }],
        execute: function() {
            run.$inject = ['$rootScope', "$http"];
            exports_1("default",run);
        }
    }
});
//# sourceMappingURL=run.js.map