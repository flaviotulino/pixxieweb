import LoginController from "./controllers/LoginController";
class Routes {
    public static routes = [
        {
            name: 'login',
            url: '/login',
            controller: LoginController
        },
        {
            name: 'dashboard',
            url: '',
            templateUrl: '/views/dashboard.html',
            abstract: true
        },
        {
            name: 'dashboard.home',
            url: '/'
        }
    ];

    public static otherwise = "/";
}

export default Routes;


