System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    ///<reference path="services/AuthService.ts"/>
    function customRun($rootScope, AuthService) {
        $rootScope.AuthService = AuthService;
    }
    return {
        setters:[],
        execute: function() {
            customRun.$inject = ['$rootScope', "AuthService"];
            exports_1("default",customRun);
        }
    }
});
//# sourceMappingURL=customRun.js.map