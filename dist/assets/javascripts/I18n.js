System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var I18n;
    return {
        setters:[],
        execute: function() {
            /**
             * Use this class to provide interpolation when needed
             */
            I18n = (function () {
                function I18n() {
                }
                I18n.t = function (string, args) {
                    if (angular.isDefined(args)) {
                        var keys = Object.keys(args);
                        // For each argument passed as object
                        keys.map(function (key) {
                            // replace %{SOME_KEY_HERE} with its value
                            var expr = new RegExp("\%\{" + key + "\}");
                            string = string.replace(expr, args[key]);
                        });
                    }
                    return string;
                };
                return I18n;
            }());
            exports_1("default",I18n);
        }
    }
});
//# sourceMappingURL=I18n.js.map