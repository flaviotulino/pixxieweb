/**
 * Defines the <page> tag which created a section using the state name as reference
 */
var directives;
(function (directives) {
    function page() {
        return {
            restrict: "E",
            transclude: true,
            replace: true,
            template: "<section class='page {{page}}-view' ng-transclude></section>",
            link: function () {
            }
        };
    }
    directives.page = page;
})(directives || (directives = {}));
//# sourceMappingURL=page.js.map